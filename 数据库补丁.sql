/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : 新数据库

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2016-09-09 01:40:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `t_sys_module`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_module`;
CREATE TABLE `t_sys_module` (
  `module_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `controller_caption` varchar(50) DEFAULT NULL,
  `controller_name` varchar(50) DEFAULT NULL,
  `controller_path` varchar(250) DEFAULT NULL,
  `charset` varchar(50) DEFAULT NULL,
  `method_func` varchar(50) DEFAULT NULL,
  `data_type` smallint(10) DEFAULT '1',
  `html_style` smallint(50) DEFAULT '1',
  `javascript_core` smallint(4) DEFAULT NULL,
  `javascript_file` smallint(4) DEFAULT NULL,
  `version` varchar(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `setting` text,
  `submit_type` smallint(10) DEFAULT '2',
  `user_id` int(11) DEFAULT '0',
  `modified` datetime DEFAULT NULL,
  `extend_class` varchar(50) DEFAULT NULL,
  `error_tips` varchar(50) DEFAULT NULL,
  `required_tips` varchar(50) DEFAULT NULL,
  `page_size` smallint(6) DEFAULT NULL,
  `relation_module` text,
  `file_value_from` varchar(250) DEFAULT NULL,
  `field_name_format` smallint(1) DEFAULT '0',
  `js_path` varchar(250) DEFAULT 'scripts',
  `css_icon` varchar(50) DEFAULT 'fa fa-dropbox',
  `controller_author` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_sys_module
-- ----------------------------
INSERT INTO `t_sys_module` VALUES ('1', '测量结果', 'Custom1111', 'Adminpanel/', 'utf-8', 'add,del,edit,readonly,sort,lists,search', '1', '1', '3', '2', '1', '2016-09-09 00:31:41', null, '2', '5', '2016-09-09 00:31:41', 'Admin_Controller', null, null, '10', null, null, '3', 'scripts/adminpanel/', 'bookmark', 'darden');
INSERT INTO `t_sys_module` VALUES ('2', '测试', 'Custom1111222', 'Adminpanel/', 'utf-8', 'add,del,edit,readonly,sort,lists,search', '1', '1', '3', '2', '1', '2016-09-09 00:37:59', null, '2', '5', '2016-09-09 00:37:59', 'Admin_Controller', null, null, '10', null, null, '3', 'scripts/adminpanel/', 'bookmark', 'darden');
INSERT INTO `t_sys_module` VALUES ('3', '测试', 'Custom1111111', 'Adminpanel/', 'utf-8', 'add,del,edit,readonly,sort,lists,search', '1', '1', '3', '2', '1', '2016-09-09 00:38:42', null, '2', '5', '2016-09-09 00:38:42', 'Admin_Controller', '%s不合要求', '%S不能为空', '10', 'null', null, '3', 'scripts/adminpanel/', 'mode', 'darden');

-- ----------------------------
-- Table structure for `t_sys_module_datasource`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_module_datasource`;
CREATE TABLE `t_sys_module_datasource` (
  `datasource_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `datasource_typeid` smallint(4) DEFAULT '1',
  `module_id` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT '0',
  `setting` text,
  `datasource_name` varchar(50) DEFAULT NULL,
  `sql_fields` varchar(250) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `concat_char` varchar(10) DEFAULT NULL,
  `datasource_function_name` varchar(50) DEFAULT NULL,
  `controller_path` varchar(250) DEFAULT NULL,
  `sql_fields_caption` varchar(250) DEFAULT NULL,
  `force_convert_text_edit` tinyint(1) DEFAULT '0',
  `force_convert_text_readonly` tinyint(1) DEFAULT '0',
  `sql_convert_fields` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`datasource_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_sys_module_datasource
-- ----------------------------

-- ----------------------------
-- Table structure for `t_sys_module_field`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_module_field`;
CREATE TABLE `t_sys_module_field` (
  `field_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(11) DEFAULT NULL,
  `field_name` varchar(50) DEFAULT NULL,
  `field_caption` varchar(50) DEFAULT NULL,
  `field_type` smallint(4) DEFAULT NULL,
  `field_options` varchar(200) DEFAULT NULL,
  `default_value` varchar(50) DEFAULT NULL,
  `is_sort` tinyint(1) DEFAULT '0',
  `is_search` tinyint(1) DEFAULT '0',
  `is_list` tinyint(1) DEFAULT '0',
  `index_num` int(11) DEFAULT '0',
  `remark` varchar(250) DEFAULT NULL,
  `is_required` tinyint(1) DEFAULT '0',
  `error_tips` varchar(150) DEFAULT NULL,
  `required_tips` varchar(150) DEFAULT NULL,
  `is_key` tinyint(1) DEFAULT '0',
  `upload_path` varchar(50) DEFAULT NULL,
  `upload_name` varchar(50) DEFAULT NULL,
  `upload_max_size` int(4) DEFAULT '0',
  `upload_file_type` varchar(50) DEFAULT NULL,
  `upload_url` varchar(250) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_pri` tinyint(1) DEFAULT '0',
  `group_item_num` smallint(4) DEFAULT '0',
  `is_options_from_datasource` tinyint(1) DEFAULT '0',
  `datasource_model` varchar(250) DEFAULT NULL,
  `datasource_control` varchar(250) DEFAULT NULL,
  `datasource_control_path` varchar(250) DEFAULT NULL,
  `datasource_function_name` varchar(50) DEFAULT NULL,
  `is_unique` tinyint(1) DEFAULT '0',
  `datasource_id` smallint(4) DEFAULT '0',
  PRIMARY KEY (`field_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_sys_module_field
-- ----------------------------
INSERT INTO `t_sys_module_field` VALUES ('1', '3', 'custom1111111_id', 'ID', '0', null, '', '0', '0', '0', '1', null, '0', 'ID不合要求', '不能为空', '0', '', null, '1024', '', null, '2016-09-09 00:00:00', '2016-09-09 00:55:47', '5', '1', '1', '0', 'Custom1111111_model', 'Custom1111111', 'Adminpanel//Custom1111111', '', '0', '0');
INSERT INTO `t_sys_module_field` VALUES ('2', '3', 'dddd', '客户名称', '5', null, '1', '1', '1', '1', '0', null, '1', '客户名称不合要求', '不能为空', '0', '', null, '1024', '', null, '2016-09-09 00:00:00', '2016-09-09 00:55:47', '5', '0', '0', '0', 'Custom1111111_model', 'Custom1111111', 'Adminpanel//Custom1111111', '', '0', '0');

-- ----------------------------
-- Table structure for `t_sys_module_log`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_module_log`;
CREATE TABLE `t_sys_module_log` (
  `log_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `logo_content` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_sys_module_log
-- ----------------------------

-- ----------------------------
-- Table structure for `t_sys_module_trigger`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_module_trigger`;
CREATE TABLE `t_sys_module_trigger` (
  `trigger_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(11) DEFAULT NULL,
  `var_code` text,
  `construct_code` text,
  `insert_before_code` text,
  `insert_after_code` text,
  `update_before_code` text,
  `update_after_code` text,
  `delete_before_code` text,
  `delete_after_code` text,
  `user_id` int(11) DEFAULT NULL,
  `view_header_code` text,
  `view_footer_code` text,
  PRIMARY KEY (`trigger_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_sys_module_trigger
-- ----------------------------
