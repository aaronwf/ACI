<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * AutoCodeIgniter.com
 *
 * 基于CodeIgniter核心模块自动生成程序
 *
 * 源项目		AutoCodeIgniter
 * 作者：		AutoCodeIgniter.com Dev Team
 * 版权：		Copyright (c) 2015 , AutoCodeIgniter com.
 * 项目名称：图书信息管理 MODEL
 * 版本号：1 
 * 最后生成时间：2016-10-31 21:40:09 
 */
class Book_model extends Base_Model {
	
    var $page_size = 10;
    function __construct()
	{
    	$this->db_tablepre = 't_aci_';
    	$this->table_name = 'book';
		parent::__construct();
	}
    
    /**
     * 初始化默认值
     * @return array
     */
    function default_info()
    {
    	return array(
		'book_id'=>0,
		'book_name'=>'',
		'book_author'=>'',
		'book_no'=>'',
		'book_public_date'=>'',
		'user_id'=>'',
		'createtime'=>'',
		);
    }
    
    /**
     * 安装SQL表
     * @return void
     */
    function init()
    {
    	$this->query("CREATE TABLE  IF NOT EXISTS `t_aci_book`
(
`book_name` varchar(250) DEFAULT NULL COMMENT '书名',
`book_author` varchar(250) DEFAULT NULL COMMENT '作者',
`book_no` varchar(250) DEFAULT NULL COMMENT 'ISDN',
`book_public_date` date DEFAULT NULL COMMENT '发布时间',
`user_id` varchar(50) DEFAULT NULL COMMENT '发布人',
`createtime` varchar(50) DEFAULT NULL COMMENT '创建日期',
`book_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`book_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
");
    }
    
        
}

// END book_model class

/* End of file book_model.php */
/* Location: ./book_model.php */
?>