<?php defined('BASEPATH') or exit('No direct script access allowed.'); ?><?php defined('BASEPATH') or exit('No permission resources.'); ?>
<div class='panel panel-default '>
    <div class='panel-heading'>
        <i class='fa fa-table'></i> 图书信息管理 查看信息 
        <div class='panel-tools'>
            <div class='btn-group'>
            	<a class="btn " href="<?php echo base_url('adminpanel/book')?>"><span class="glyphicon glyphicon-arrow-left"></span> 返回 </a>
            </div>
        </div>
    </div>
    <div class='panel-body '>
<div class="form-horizontal"  >
	<fieldset>
        <legend>基本信息</legend>
     
  	  	
	<div class="form-group">
				<label for="book_name" class="col-sm-2 control-label form-control-static">书名</label>
				<div class="col-sm-9 form-control-static ">
					<?php echo isset($data_info['book_name'])?$data_info['book_name']:'' ?>
				</div>
			</div>
	  	
	<div class="form-group">
				<label for="book_author" class="col-sm-2 control-label form-control-static">作者</label>
				<div class="col-sm-9 form-control-static ">
					<?php echo isset($data_info['book_author'])?$data_info['book_author']:'' ?>
				</div>
			</div>
	  	
	<div class="form-group">
				<label for="book_no" class="col-sm-2 control-label form-control-static">ISDN</label>
				<div class="col-sm-9 form-control-static ">
					<?php echo isset($data_info['book_no'])?$data_info['book_no']:'' ?>
				</div>
			</div>
	  	
	<div class="form-group">
				<label for="book_public_date" class="col-sm-2 control-label form-control-static">发布时间</label>
				<div class="col-sm-9 form-control-static ">
					<?php echo isset($data_info['book_public_date'])?$data_info['book_public_date']:'' ?>
				</div>
			</div>
	  	
	<div class="form-group">
				<label for="user_id" class="col-sm-2 control-label form-control-static">发布人</label>
				<div class="col-sm-9 form-control-static ">
					<?php echo isset($data_info['user_id'])?$data_info['user_id']:'' ?>
				</div>
			</div>
	  	
	<div class="form-group">
				<label for="createtime" class="col-sm-2 control-label form-control-static">创建日期</label>
				<div class="col-sm-9 form-control-static ">
					<?php echo isset($data_info['createtime'])?$data_info['createtime']:'' ?>
				</div>
			</div>
	    </fieldset>
	</div>
</div>
</div>
